import shutil
from sys import exit
import sys,os
from sklearn.model_selection import train_test_split


<<<<<<< HEAD
InputFileFolder = '/largehome/dtyu/cell_images'
=======
InputFileFolder = '/Users/dantongyu/Desktop/Git/cell_images'
>>>>>>> 2704cc8f535c2f0a5791e725c309733176603b27


try:
    os.chdir(InputFileFolder)
except OSError:
    print ("The Input File Folder Does not Exist!" % InputFileFolder)
    print ("Please download the cell images and set up the InputFileFolder!")
    exit(1)
else:
    print ("Successfully Confirm the directory %s " % InputFileFolder)


parasitized_data = os.listdir(InputFileFolder + "/Parasitized/")
uninfected_data = os.listdir(InputFileFolder + "/Uninfected/")

if len(parasitized_data) ==0: 
    print("The positive samples are missing\n")

if len(uninfected_data) ==0:
    print("The negative samples are missing\n")


filenames = []
labels = []


for img in parasitized_data:
    imageName =  img
    if (bool('.png' in imageName)):
        filenames.append(imageName)
        labels.append(1)

    
for img in uninfected_data:
    imageName = img
    if (bool('.png' in imageName)):
        filenames.append(imageName)
        labels.append(0)

print(len(filenames))

TrainingData, ValidationData, Traininglabels, Validationlabels = train_test_split(filenames, labels, train_size = 22045, test_size =5513, random_state = 101)

#idx = np.arange(len(filenames))
#np.random.shuffle(idx)
#TrainingData = [filenames[k] for k in idx[0:22045]]
#Traininglabels = [labels[k] for k in  idx[0:22045]]
#ValidationData = [filenames[k] for k in idx[22045:,]]
#Validationlabels = [labels[k] for k in idx[22045:,]]
#print((TrainingData))
#print((ValidationData))


try:
    path = InputFileFolder + '/../f1_mal'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    

try:
    path = InputFileFolder + '/../f1_mal/train'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    
    
try:
    path = InputFileFolder + '/../f1_mal/train/1-Parasitized'
    os.mkdir(path)
    
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)

try:
    path = InputFileFolder + '/../f1_mal/train/0-Uninfected'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    

for i in range(len(TrainingData)):
    
    if Traininglabels[i]==1:
    # patient is infected
        source = InputFileFolder + "/Parasitized/"+TrainingData[i]
        target = InputFileFolder + "/../f1_mal/train/1-Parasitized/"+TrainingData[i]
    else:
    # normal
        source = InputFileFolder + "/Uninfected/"+TrainingData[i]
        target = InputFileFolder + "/../f1_mal/train/0-Uninfected/"+TrainingData[i]
  
    try:
        shutil.copy(source, target)
    except IOError as e:
        print("Unable to copy file. %s" % e)
        exit(1)
    except:
        print("Unexpected error:", sys.exc_info())
        exit(1)
    

try:
    path = InputFileFolder + '/../f1_mal/valid'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    
try:
    path = InputFileFolder + '/../f1_mal/valid/1-Parasitized'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    

try:
    path = InputFileFolder + '/../f1_mal/valid/0-Uninfected'
    os.mkdir(path)
except OSError:
    print ("Creation of the directory %s failed" % path)
else:
    print ("Successfully created the directory %s " % path)
    

    
for i in range(len(ValidationData)):
    
    if Validationlabels[i]==1:
    # patient is infected
        source = InputFileFolder + "/Parasitized/"+ValidationData[i]
        target = InputFileFolder + "/../f1_mal/valid/1-Parasitized/"+TrainingData[i]
    else:
    # normal
        source = InputFileFolder + "/Uninfected/"+ValidationData[i]
        target = InputFileFolder + "/../f1_mal/valid/0-Uninfected/"+ValidationData[i]
  
    try:
        shutil.copy(source, target)
    except IOError as e:
        print("Unable to copy file. %s" % e)
        exit(1)
    except:
        print("Unexpected error:", sys.exc_info())
        exit(1)   
